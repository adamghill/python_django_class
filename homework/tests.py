import unittest
from one import add
from two import square
from three import count
from four import filter


class One(unittest.TestCase):
    def test_adding_floats(self):
        actual = add(3.4, 6.7)
        self.assertEqual(actual, 10.1)


    def test_adding_float_and_integer(self):
        actual = add(2, 3.4)
        self.assertEqual(actual, 5.4)


    def test_adding_strings(self):
        actual = add("hello ", "world")
        self.assertEqual(actual, "hello world")


    def test_adding_strings_2(self):
        actual = add("The Motley ", "Fool")
        self.assertEqual(actual, "The Motley Fool")


class Two(unittest.TestCase):
    def test_square(self):
        actual = square([2, 4, 5])
        self.assertEqual(actual, [4, 16, 25])


    def test_square_2(self):
        actual = square([2.4, 4, 5.5])
        self.assertEqual(actual, [5.76, 16, 30.25])


class Three(unittest.TestCase):
    def test_integers(self):
        actual = count([2, 3, 4, 5, 3, 3, 3], 3)
        self.assertEqual(actual, 4)


    def test_integers_2(self):
        actual = count([2, 3, 4], 4)
        self.assertEqual(actual, 1)


    def test_integers_3(self):
        actual = count([2, 3, 4], 5)
        self.assertEqual(actual, 0)


    def test_strings(self):
        actual = count(["a", "b", "c", "b"], "b")
        self.assertEqual(actual, 2)


    def test_strings_2(self):
        actual = count(["hello", "bye", "hello"], "hi")
        self.assertEqual(actual, 0)


class Four(unittest.TestCase):
    def test_integers(self):
        actual = filter([2, 3, 4, 5, 6, 6], 6)
        self.assertEqual(actual, [2, 3, 4, 5])


    def test_strings(self):
        actual = filter(['a', 'b', 'c', 'd'], 'a')
        self.assertEqual(actual, ['b', 'c', 'd'])


    def test_strings_2(self):
        actual = filter(['a', 'b', 'c', 'd'], 'x')
        self.assertEqual(actual, ['a', 'b', 'c', 'd'])




if __name__ == '__main__':
    unittest.main()