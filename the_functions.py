
#Static Methods
# class some:
#    @staticmethod
#    def f():
#        print "I am a static method"

# some.f()

# #Scoping
# def f():
#    print "outer function"
#    x = 3
#    def e():
#        #x = 4
#        print "inner function"
#        print x
#        print y

#    y = 5
#    e()
# f()

#Functions are objects
# def f():
#    """
#    I am docs
#    """
#    print "I am simple function"

# print isinstance(f, object)
# print id(f)  # each object has unique Id
# print f.func_doc
# print f.func_name

# #Function return more than one value
# n = [1, 2, 3, 4, 5]
# def f(n):
#    return max(n), min(n)

# max, min = f(n)
# print max, min


# #Functions can have arbitary params
# def sum(*args):
#   r = 0
#   for i in args:
#      r += i
#   return r

# print sum(1, 2, 3)
# print sum(1, 2, 3, 4, 5)

#Modules
# from mymodule.mymath import *
# import mymodule.mymath
# from mymodule.mymath import add
# from mymodule.mymath import subtract
# from mymodule.mymath import notthere

# print mymodule.mymath.add(15, 16)
# print add(15,16)
# print subtract(100,60)
# print multiply(100,60)


# python mymodule/anothermodule/cat.py

# Packages
# add __init__.py to work correctly
# from mymodule.animal import speak

# print speak()



#### Solutions

#def makes10(a, b):
#    return (a == 10 or b == 10 or a+b == 10)

# def sum(a,b=10):
#   return a+b

# print sum(2)
# print sum(2,4)
# print sum(100,100)


# def multiply(nums):
#     return [x*2 for x in nums]


# print multiply([2,2,2])
# print multiply([2,3,4])
# print multiply([5,6,7])
# print multiply([2.2,3.2,4.3])

# def min_max(nums):
#    return max(nums), min(nums)

# max_num, min_num = min_max([2,3,4])
# print max_num, min_num
# print min_max([12,13,4])
# print min_max([22,33,14])
# print min_max([5,5,4])