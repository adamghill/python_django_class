import unittest


class Instrument:
	def __init__(self, id, exchange, symbol):
		self.id = id
		self.exchange = exchange
		self.symbol = symbol

	def get_nice_name(self):
		return '%s:%s' % (self.exchange, self.symbol)

	def __unicode__(self):
		return str(self.id)

	def __str__(self):
		return self.__unicode__()

	@staticmethod
	def validate_exchange(exchange):
		if exchange == 'NASDAQ':
			return True

		return False


class TestInstrument(unittest.TestCase):
	def setUp(self):
		self.instrument = Instrument(1, 'NASDAQ', 'AAPL')


	def test_instrument_dictionary(self):
		self.assertTrue('symbol' in self.instrument.__dict__)


	def test_instrument_hasattr(self):
		self.assertTrue(hasattr(self.instrument, 'symbol'))


	def test_instrument_getattr(self):
		symbol = getattr(self.instrument, 'symbol')
		self.assertTrue(symbol == 'AAPL')


	def test_instrument_add_attribute(self):
		self.instrument.is_valid = True
		self.assertTrue(self.instrument.is_valid)


	def test_instrument_add_function(self):
		self.instrument.is_valid = lambda: self.instrument.symbol == 'AAPL'
		self.assertTrue(self.instrument.is_valid())


	def test_instrument_to_string(self):
		instrument_to_string = str(self.instrument)
		self.assertTrue(instrument_to_string == '1')


	def test_get_nice_name(self):
		nice_name = self.instrument.get_nice_name()
		self.assertTrue(nice_name == 'NASDAQ:AAPL')


	def test_validate_exchange_add_one(self):
		# Expect to add another if/else in class
		valid_exchange = Instrument.validate_exchange('NASDAQ')
		self.assertTrue(valid_exchange)


	def test_validate_exchange_add_many(self):
		# Expect to add list to look through in class
		exchanges = ['NASDAQ', 'NASDAQ', 'NASDAQ', 'NASDAQ', 'NASDAQ', 'NASDAQ', 'NASDAQ', 'NASDAQ', 'NASDAQ', 'NASDAQ', 'NASDAQ']

		for exchange in exchanges:
			valid_exchange = Instrument.validate_exchange(exchange)
			self.assertTrue(valid_exchange)


	def test_array_of_instruments(self):
		apple = Instrument(1, 'NASDAQ', 'AAPL')
		google = Instrument(2, 'NASDAQ', 'GOOG')
		instruments = [apple, google]
		
		self.assertTrue(len(instruments) == 2)


if __name__ == '__main__':
	unittest.main()
